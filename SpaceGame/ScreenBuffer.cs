﻿using System;

namespace ConsoleGame
{
    public class ScreenBuffer
    {
        // Set console window size
        public static int X = 100;
        public static int Y = 45;
        public static char[,] screenBufferArray = new char[X, Y - statusBar];
        public static string screenBuffer;
        public static Char[] arr; // temporary array
        public static int i = 0; // place tracker in the array
        public static int statusBar = 4; // height of player status bar

        // meetod, mis küsib stringi ning x ja y koordinaate
        public static void Draw(string text, int x, int y)
        {
            // text into array
            arr = text.ToCharArray(0, text.Length);

            // iterate through array, adding values to buffer
            i = 0;
            foreach (char c in arr)
            {
                screenBufferArray[x + i, y] = c;
                i++;
            }
        }

        public static void DrawScreen()
        {
            screenBuffer = "";
            // iterate through buffer, adding each value to screenbuffer
            for (int y = 0; y < Y - statusBar; y++)
            {
                for (int x = 0; x < X; x++)
                {
                    screenBuffer += screenBufferArray[x, y];
                }
            }

            // set cursor to starting position
            Console.SetCursorPosition(0, 0);
            // draw the screen
            Console.Write(screenBuffer);
            // empty the buffer array
            screenBufferArray = new char[X, Y - statusBar];
        }
    }
}
