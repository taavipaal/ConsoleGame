﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleGame
{
    class Action
    {
        public static void Combat(Character player, Character enemy)
        {
            Random r = new Random();
            int dieRollPlayer;
            int dieRollEnemy;
            // neil on hitpoints
            // neil on toughness (nt 10 taset, iga taseme juures võtab üks pihtalook 1 HP maha)
            // toughness on ka AC, nt toughness 1 tähendab, et peab saama tohit 10 + lvl
            // neil on level - löögi hittimise tõenäosus tuleneb nt levelist (nt lvl 1 juures 25% ja 2-7% tõuseb per level)


            // Siin lvl 1 koll kellel on 1 toughness saab pihta 2.5 lööki 10st

            // Kes lööb esimesena
            int firstStrike = r.Next(0, 2);

            // die rolls on d100
            dieRollPlayer = (int)((r.Next(1, 100)) + ((25 + player.Level) * 0.5)); // 0.5 is level modifier
            dieRollEnemy = (int)((r.Next(1, 100)) + ((25 + player.Level) * 0.5));

            if (firstStrike == 0 && dieRollPlayer >= 20 + enemy.Level)
            {
                enemy.HP--;
            }
            else if (firstStrike == 1 && dieRollEnemy >= 20 + player.Level)
            {
                player.HP--;
            }

            Console.SetCursorPosition(50, ScreenBuffer.Y - 2);
            Console.Write("Enemy HP: " + enemy.HP); // max 7 chars long
            Console.SetCursorPosition((50 + 13 + 4), ScreenBuffer.Y - 3);
            Console.Write("Player rolled: " + dieRollPlayer.ToString().PadLeft(3));
            Console.SetCursorPosition((50 + 13 + 4), ScreenBuffer.Y - 2);
            Console.Write("Enemy rolled: " + dieRollEnemy.ToString().PadLeft(3));
            
        }     
        

        // FOR TESTING
        public static void Test(object x)
        {
            Console.SetCursorPosition(0, 0);
            Console.Write(x);
        }
        public static void Test(object x, int pos)
        {
            Console.SetCursorPosition(0, pos);
            Console.Write(x);
        }

        public static void Test(object x, object y)
        {
            Console.SetCursorPosition(0, 0);
            Console.Write(x);
            Console.SetCursorPosition(0, 1);
            Console.Write(y);
        }

        public static void Test(object x, object y, int posx, int posy)
        {
            Console.SetCursorPosition(0, posx);
            Console.Write(x);
            Console.SetCursorPosition(0, posy);
            Console.Write(y);
        }

        
    }
}
