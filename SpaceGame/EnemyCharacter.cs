﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace ConsoleGame
{
    public class EnemyCharacter : Character
    {
        int LineOfSight = 15; // number of chars the enemy can "see"
        public int XPWorth = 10; // how much XP is the enemy worth
        int timeSiceLastMove;
        public int TimeBetweenMoves = 300; // enemy speed in milliseconds

        // Enemy constructor
        // Default is 1 LVL enemy
        public EnemyCharacter(int x, 
                              int y, 
                              string symbol, 
                              int hp = 1, 
                              int level = 1, 
                              int toughness = 1, 
                              int speed = 400, 
                              int xpworth = 10, 
                              int lineofsight = 15) : base(x, y, hp, symbol, level, toughness)
        {
            TimeBetweenMoves = speed;
            XPWorth = xpworth;
            LineOfSight = lineofsight;
        }

        override public void Update(int deltaTimeMS, Character player, Character enemy)
        {
            // How much time has elapsed
            timeSiceLastMove += deltaTimeMS;
            
            // Has enough time elapsed to move
            if (timeSiceLastMove < TimeBetweenMoves)
            {
                // Exit the method
                return;
            }

            // Keeps the time difference
            timeSiceLastMove -= TimeBetweenMoves;

            #region EnemyAI
            Point playerXY = new Point(player.X, player.Y);
            Point enemyXY = new Point(X, Y);
            int distance = CalcDist(playerXY, enemyXY); // distance between enemy and player

            int deltaX = player.X - X;
            int deltaY = player.Y - Y;
                     
            if (Math.Abs(distance) < LineOfSight)
            {
                EnemyMoves(deltaX, deltaY, distance);
                base.Update(deltaTimeMS, player, enemy);
            }

            if (Math.Abs(distance) < 2)
            {
                Action.Combat(player, enemy);
            }
            #endregion

        }

        // Calculates the distance between characters
        int CalcDist(Point player, Point enemy)
        {
            int result = (int)Math.Sqrt(Math.Pow(player.X - enemy.X, 2.0) + Math.Pow(player.Y - enemy.Y, 2.0));
            return result;
        }
        
        void EnemyMoves(int deltaX, int deltaY, int distance)
        {
            // Combat distance
            bool dist = distance > 1;

            if (deltaX < 0 && deltaY < 0 && dist)
            {
                Y--;
                X--;
            }
            else if (deltaX > 0 && deltaY < 0 && dist)
            {
                Y--;
                X++;
            }
            else if (deltaX > 0 && deltaY > 0 && dist)
            {
                Y++;
                X++;
            }
            else if (deltaX < 0 && deltaY > 0 && dist)
            {
                Y++;
                X--;
            }
            else if (deltaX == 0 && deltaY > 0 && dist)
                Y++;
            else if (deltaX == 0 && deltaY < 0 && dist)
                Y--;
            else if (deltaY == 0 && deltaX > 0 && dist)
                X++;
            else if (deltaY == 0 && deltaX < 0 && dist)
                X--;            
        }
    }
}