﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Threading;

namespace ConsoleGame
{
    public class PlayerCharacter : Character
    {
        public PlayerCharacter(int x, int y, int hp, string symbol = "@") : base(x, y, hp, symbol)
        {

        }

        ConsoleKeyInfo k;
        int XP = 0;
        
        override public void Update(int deltaTimeMS, Character player, Character enemy)
        {
            //Console.SetCursorPosition(30, 0);
            //Console.Write("UX: " + enemyX);
            //Console.SetCursorPosition(30, 1);
            //Console.Write("UY: " + enemyY);

            if (Console.KeyAvailable)
            {
                k = Console.ReadKey(true);
                GetInput(k);

                // kui base update teeb midagi siis siin saab selle välja kutsuda
                base.Update(deltaTimeMS, player, enemy);
            }

        }

        override public void DrawStatusbar()
        {
            Console.SetCursorPosition(4, ScreenBuffer.Y - 2);
            Console.Write("HP: " + HP.ToString().PadRight(3)); // max 7 chars long
            Console.SetCursorPosition((4 + 7 + 4), ScreenBuffer.Y - 2);
            Console.Write("XP: " + XP); // max 5 chars long
            Console.SetCursorPosition((4 + 7 + 4 + 5 + 4), ScreenBuffer.Y - 2);
            Console.Write("Level: " + Level);
        }

        void GetInput(ConsoleKeyInfo k)
        {
            switch (k.Key)
            {
                case ConsoleKey.UpArrow:
                    if (Y <= 0)
                        Y = 0;
                    else
                        Y--;
                    break;
                case ConsoleKey.DownArrow:
                    if (Y >= ScreenBuffer.Y - 1 - ScreenBuffer.statusBar)
                        Y = ScreenBuffer.Y - 1 - ScreenBuffer.statusBar;
                    else
                        Y++;
                    break;
                case ConsoleKey.LeftArrow:
                    if (X <= 0)
                        X = 0;
                    else
                        X--;
                    break;
                case ConsoleKey.RightArrow:
                    if (X >= ScreenBuffer.X - 1)
                        X = ScreenBuffer.X - 1;
                    else
                        X++;
                    break;
            }
        }
    }
}
