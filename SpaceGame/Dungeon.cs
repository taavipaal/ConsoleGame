﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleGame
{
    class Dungeon
    {

        public Dungeon()
        {
            DrawMap();
        }

        // Empty map
        public char[,] Map =  new char[ScreenBuffer.X, ScreenBuffer.Y - ScreenBuffer.statusBar];

        void DrawMap()
        {
            Map[0,0] = ' ';

            for (int i = 0; i < ScreenBuffer.Y - ScreenBuffer.statusBar; i++)
            {
                for (int j = 0; j < ScreenBuffer.X; j++)
                {
                    Map[j, i] = 'x';
                }
            }
        }

        public void DisplayMap()
        {
            for (int y = 0; y < ScreenBuffer.Y - ScreenBuffer.statusBar; y++)
            {
                for (int x = 0; x < ScreenBuffer.X; x++)
                {
                    Console.Write(Map[x, y]);
                }
            }
        }
        
    }
}
