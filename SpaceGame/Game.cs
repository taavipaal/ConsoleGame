﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Linq;

namespace ConsoleGame
{
    public class Game
    {
        // starting variables
        Stopwatch stopwatch; // stopper, et aega mõõta
        //bool gameOver = false;
        public Character player;
        List<Character> Enemies = new List<Character>();

        // game constructor
        public Game()
        {
            Console.SetBufferSize(ScreenBuffer.X, ScreenBuffer.Y); // Disable scrolling
            Console.SetWindowSize(ScreenBuffer.X, ScreenBuffer.Y); // ScreenBuffer class has main console size controls
            Console.CursorVisible = false;
            
            stopwatch = new Stopwatch();

            // Create the Player
            player = new PlayerCharacter(1, 30, 100);

            // Create enemies
            Enemies.AddRange(new List<EnemyCharacter>
            {
                new EnemyCharacter(10,10,"X",5),
                new EnemyCharacter(26,33,"X",5),
                new EnemyCharacter(87,2,"X",5),
                new EnemyCharacter(0, ScreenBuffer.Y - 1, "", 10000000, lineofsight: 0) // empty enemy to keep the list alive
            });

            // Draw statusbar separator
            Console.SetCursorPosition(0, ScreenBuffer.Y - 4);
            for (int i = 0; i < ScreenBuffer.X; i++)
            {
                Console.Write("-");
            }

            // Draw the statusbar
            player.DrawStatusbar();
        }

        // game loop
        public void Run()
        {
            stopwatch.Start();
            long timeAtPreviousFrame = stopwatch.ElapsedMilliseconds;
            bool GameOver = false;

            // main game loop
            while (!GameOver)
            {
                // Time one frame should take
                int deltaTimeMS = (int)(stopwatch.ElapsedMilliseconds - timeAtPreviousFrame);
                timeAtPreviousFrame = stopwatch.ElapsedMilliseconds;

                // Track player HP change
                int prevHP = player.HP;
                int prevX = player.X;
                int prevY = player.Y;

                // peab ümber tegema
                foreach (EnemyCharacter enemy in Enemies)
                {

                    player.Update(deltaTimeMS, player, enemy);
                    enemy.Update(deltaTimeMS, player, enemy);
                    
                    // Update Player statusbar only if something changes
                    if (player.HP < prevHP) // level and xp also here later
                        player.DrawStatusbar();
                    
                    player.Draw();
                    enemy.Draw();

                    // If enemy is killed, remove it from screen
                    if (enemy.HP == 0)
                    {
                        Console.SetCursorPosition(enemy.X, enemy.Y);
                        Console.Write(" ");
                    }
                }

                // Remove enemy permanently
                Enemies.RemoveAll(x => x.HP == 0);

                // Prevent Player and enemy from overlapping
                if(Enemies.Select(x => x.X).Contains(player.X) && Enemies.Select(x => x.Y).Contains(player.Y))
                {
                    player.X = prevX;
                    player.Y = prevY;
                }
                
                // tiny sleep to avoid running too high fps
                Thread.Sleep(5);
            }
        }
    }
}
