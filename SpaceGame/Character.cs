﻿using System;
using System.Collections.Generic;

namespace ConsoleGame
{
    abstract public class Character // abstract ei luba seda klassi üksinda kunagi välja kutsuda
    {
        // starting position
        int _X;
        int _Y;
        public string Symbol; // character
        int _HP; // hitpoints
        int XP = 0; // experience points
        public int Level = 1; // character level
        int Toughness = 1; // how many HP opponent loses per round

        // specify starting position and character symbol
        public Character(int x, int y, int hp, string symbol = "@", int toughness = 1, int xp = 0, int level = 1)
        {
            X = x;
            Y = y;
            _HP = hp;
            XP = xp;
            Level = level;
            Toughness = toughness;
            Symbol = symbol;
        }

        public int X
        {
            get => _X;
            set
            {
                if (value < 0 || value > ScreenBuffer.X - 1)
                {
                    throw new Exception("X coordinate is out of bounds.");
                }
                _X = value;
            }
        }
        public int Y
        {
            get => _Y;
            set
            {
                if (value < 0 || value > ScreenBuffer.Y - 1)
                {
                    throw new Exception("Y coordinate is out of bounds.");
                }
                _Y = value;
            }
        }
        public int HP
        {
            get => _HP;
            set => _HP = value;
        }
        virtual public void Update(int deltaTimeMS, Character player, Character enemy)
        {
            // virtual lubab üle kirjutada seda meetodit, aga jääb ise ka alles
            // ning teda saab ka individuaalselt välja kutsuda
            // abstract meetodi aga PEAB inherited classides üle kirjutama
            // ning seda ei saa kunagi iseseisvalt välja kutsuda

            // Joonistab nii playeri kui vastased korraga ekraanile
            ScreenBuffer.Draw(Symbol, X, Y);
            ScreenBuffer.DrawScreen();

        }

        virtual public void DrawStatusbar()
        {
            // EMPTY
        }

        public void Draw()
        {
            Console.SetCursorPosition(X, Y);
            Console.Write(Symbol);
        }
    }
}
