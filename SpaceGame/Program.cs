﻿using System;

namespace ConsoleGame
{
    class Program
    {
        static void Main(string[] args)
        {

            // KOMMENTAARID
            // 1) Console.Clear() ei tohi kasutada, kuna see paneb pildi vilkuma,
            //    sest siis korraks tehakse kogu konsool tühjaks
            // 2) Selle asemel peab kasutama screen bufferit, mis kirjutab lihtsalt ekraani üle
            // 3) Maci konsooli suurus on 80x24. Maci peal ei saa kasutada meetodit Console.SetWindowSize();
            // 4) Konsooli suurust 80x24 tuleb meetodite ja loopide sees kasutada
            //    Console.WindowWidth-1 ja Console.WindowHeight-1, kuna suurus 0 on ka sisse arvestatud
            //    ehk width on 0-79 (kokku 80) ja height on 0-23 (kokku 24)
            // 5) Maci peal ei tööta ka Console.SetBufferSize() ja Console.MoveBufferArea()
            // 6) Game loop töötab nii, et kõigepealt initialise game, siis update/take input, siis draw



            // TO do:
            // 1) screen flickering põhjustatud sellest, et mäng redrawb end pidevalt isegi siis, kui midagi ei liigu




            


            Console.SetBufferSize(ScreenBuffer.X, ScreenBuffer.Y); // Disable scrolling
            Console.SetWindowSize(ScreenBuffer.X, ScreenBuffer.Y); // ScreenBuffer class has main console size controls
            Console.CursorVisible = false;

            Dungeon lvl1 = new Dungeon();

            lvl1.DisplayMap();

            
            // start new game
            Game game = new Game();
            Console.ReadKey();
            // run game
            //game.Run();

            // game over
            //game.Over();

        }
    }
}
